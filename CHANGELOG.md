
# Changelog for Portal Auth Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.0.2] - 2023-03-31

- ported to git


## [v1.0.0] - 2013-01-11

- First release
